<?php
	if ($site->images()->find($site->sharing_image()->value())) {
		$imageUrl = $site->images()->find($site->sharing_image()->value())->url();
	} else {
		$imageUrl = null;
	}

?>

<meta property="og:type" content="website" />
<meta property="og:title" content="<?= $site->title()->html() ?>"/>
<meta property="og:url" content="<?= $site->url() ?>" />
<meta property="og:description" content="<?= $site->description()->html() ?>" />
<meta property="og:image" content="<?= $imageUrl ?>" />

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:creator" content="">
<meta name="twitter:title" content="<?= $site->title()->html() ?>">
<meta name="twitter:description" content="<?= $site->description()->html() ?>">
<meta name="twitter:image" content="<?= $imageUrl ?>">