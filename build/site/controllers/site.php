<?php

// This is a controller file that contains the logic for the blog template.
// It consists of a single function that returns an associative array with
// template variables.
//
// Learn more about controllers at:
// https://getkirby.com/docs/developer-guide/advanced/controllers

return function($site, $pages, $page) {
  $typefaces = page('typefaces')->children()->visible()->shuffle();
  $ppp = $site->posts_per_page()->int();
  return [
    'posts' 		=> page('posts')->children()->visible()->paginate($ppp),
    'archive' 		=> page('posts')->children()->visible(),
    'intro_text' 	=> page('home')->intro(),
    'typefaces'		=> $typefaces,
    'typeface'		=> $typefaces->first(),
  ];

};
