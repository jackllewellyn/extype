<?php snippet('header') ?>
<?php snippet('intro') ?>
<main>
	<?php snippet('post-body', array('page' => $page, 'url' => false)) ?>
</main>
<?php snippet('footer') ?>
