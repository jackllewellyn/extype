import _ from 'lodash';

const Post = (el) => {
	let scrollMin, scrollMax, titleState = 'abs-top';
	const titleEl = el.querySelector('.post__title--sticky');
	const firstTextEl = el.querySelector('.post__text');

	const cachePosition = () => {
		scrollMin = el.offsetTop;
		scrollMax = firstTextEl ? firstTextEl.offsetTop + el.offsetTop : scrollMin + el.clientHeight - titleEl.clientHeight - window.innerHeight;
		titleEl.style.top = firstTextEl ? `${firstTextEl.offsetTop}px` : `${el.clientHeight - titleEl.clientHeight}px`;
	};

	const onScroll = _.throttle(() => {
		const scrollTop = window.scrollY || document.documentElement.scrollTop;

		const newTitleState = (() => {
			if (scrollTop < scrollMin) return 'abs-top';
			if (scrollTop > scrollMax) return 'abs-bottom';
			return 'fixed-top';
		})();

		if (newTitleState !== titleState) {
			titleEl.classList.remove(`post__title--${titleState}`);
			titleEl.classList.add(`post__title--${newTitleState}`);
			titleState = newTitleState;
		}
	}, 16.666);

	const onResize = _.debounce(() => {
		cachePosition();
		requestAnimationFrame(onScroll);
	}, 111);

	onResize();
	window.addEventListener('scroll', onScroll);
	window.addEventListener('resize', onResize);
}

export default Post;

