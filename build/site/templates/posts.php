<?php snippet('header') ?>
<?php snippet('intro') ?>
<main>
	<?php foreach($posts as $post): ?>
		<?php snippet('post-body', array('page' => $post, 'url' => $post->url())) ?>
	<?php endforeach ?>
</main>
<?php snippet('pagination') ?>
<?php snippet('footer') ?>
