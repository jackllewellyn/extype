<?php if($posts->pagination()->hasPages()): ?>
<nav class="pagination typo--large">

	<?php if($posts->pagination()->hasPrevPage()): ?>
		<a class="pagination__prev" href="<?= $posts->pagination()->prevPageURL() ?>">
			<div class="pagination__link-skew">
				previous
				<span class="pagination__glyph">⮐</span>
			</div>
		</a>
	<?php else: ?>
		<span class="pagination__prev pagination__prev--disabled">
			<div class="pagination__link-skew">
				previous
				<span class="pagination__glyph">⮐</span>
			</div>
		</span>
	<?php endif ?>

	<ul class="pagination__page-links">
		<?php foreach($posts->pagination()->range(3) as $r): ?>
		<li class="pagination__page-link <?php if($posts->pagination()->page() == $r) echo ' pagination__page-link--active' ?>">
			<a
				href="<?= $posts->pagination()->pageURL($r) ?>"
			><?= $r ?></a>
		</li>
		<?php endforeach ?>
	</ul>
	
	<?php if($posts->pagination()->hasNextPage()): ?>
		<a class="pagination__next pagination__link" href="<?= $posts->pagination()->nextPageURL() ?>">
			<div class="pagination__link-skew">
				next
				<span class="pagination__glyph">⮑</span>
			</div>
		</a>
	<?php else: ?>
		<span class="pagination__next pagination__next--disabled">
			<div class="pagination__link-skew">
				next
				<span class="pagination__glyph">⮑</span>
			</div>
		</span>
	<?php endif ?>

</nav>
<?php endif ?>