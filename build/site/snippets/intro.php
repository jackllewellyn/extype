<header class="intro typo--intro">
	<h1 class="intro__accessibility-title">Ex-type</h1>
	<div class="intro__hover">
		<a class="intro__home-link" href="/">
			<?php if ($image = $typeface->images()->first()): ?>
				<img
					class="intro__title-image" alt="Ex-type"
					src="<?= $image->url() ?>"
					width="<?= $image->width() ?>"
					height="<?= $image->height() ?>"
					data-aspect-ratio="<?= $image->height() / $image->width() ?>"
				/>
			<?php endif; ?>
		</a>
		<div class="intro__text">
			<?= $intro_text->kt() ?>
			<p>
				The masthead is set in <em><span class="intro__typeface-name"><?= $typeface->title()->value() ?></span> by <?php  if ($typeface->designer_url()->isNotEmpty()) : ?><a href="<?= $typeface->designer_url()->url() ?>" target="_blank" class="intro__typeface-designer"><?= $typeface->designer()->value() ?></a> <?php else: ?><span class="intro__typeface-designer"><?= $typeface->designer()->value() ?></span><?php endif ?></em></p>
		</div>
	</div>
</header>
