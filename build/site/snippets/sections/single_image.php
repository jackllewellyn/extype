<figure class="single-image">
	<?php if ($image = $data->picture()->toFile()): ?>
		<img
			class="single-image__image"
			src=<?= $image->thumb(array('width' => 1600))->url() ?>
			srcSet="
				<?= $image->thumb(array('width' => 500))->url() ?> 500w,
				<?= $image->thumb(array('width' => 900))->url() ?> 900w,
				<?= $image->thumb(array('width' => 1600))->url() ?> 1600w,
				<?= $image->thumb(array('width' => 2200))->url() ?> 2200w,
			"
			sizes="100vw"
			alt="<?= $image->accessibility() ?>"
			width="<?= $image->width() ?>"
			height="<?= $image->height() ?>"
		/>
	<?php endif ?>
	<?php if ($data->caption()->isNotEmpty()): ?>
		<figcaption class="image-caption typo--small"><?= $data->caption->value() ?></figcaption>
	<?php endif ?>
</figure>
