<div class="footer">
  <div class="footer__index">
    <a href="/archive">
      Post Index
    </a>
  </div>
  <div class="row">
    <div class="column">
      <a href="http://rca.ac.uk" target="_blank"><img class="footer__logo" src="/assets/images/logo.svg"></a>
    </div>
    <div class="column">
      <div class="spacer"></div>
        This website is displayed in <a href="https://velvetyne.fr/fonts/sporting-grotesque/" target="_blank">Sporting Grotsque</a> by Lucas Le Bihan. <a href="https://velvetyne.fr" target="_blank">Velvetyne Foundry.</a>
    </div>
  </div>
  </a>
</div>
    <?php echo js('assets/js/main.js') ?>
</body>
</html>
