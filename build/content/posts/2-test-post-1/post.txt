Title: Test Post with a longer title

----

Author: Jack Wild

----

Date: 2018-06-10

----

Builder: 

- 
  picture: 1600x900.png
  caption: ""
  _fieldset: single_image
- 
  picture: 1600x900.png
  caption: ""
  _fieldset: single_image
- 
  picture_one: 600x700.png
  picture_two: 600x700.png
  caption: ""
  _fieldset: diptych_image
- 
  text: |
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non enim leo. Vivamus condimentum rutrum lectus, quis laoreet est porttitor sed. Nullam lacinia odio sit amet ligula tincidunt congue. In vel purus vel erat tristique laoreet ac vel arcu. Sed semper, sem sed ultricies posuere, diam nisl auctor odio, vel congue sem dui id tellus. Nam tincidunt sem vel pretium faucibus. Integer ac diam in leo porta congue.
    
    Donec ullamcorper ornare dolor ac malesuada. Suspendisse ac consequat velit, ut sodales nunc. Nunc dignissim lacus quis pulvinar tincidunt. Ut iaculis nisl nec semper efficitur. Aenean vulputate, mauris ac lobortis elementum, lectus nibh sagittis purus, ut bibendum justo velit ut nunc. Pellentesque libero turpis, feugiat in sodales sit amet, rhoncus quis eros. Duis nec ultricies nunc, in sagittis nunc.
    
    Aenean et purus odio. Mauris ut pretium velit, id commodo augue. Nam posuere, turpis quis blandit molestie, ligula ante gravida nisl, in posuere ipsum nisi a ante. Praesent auctor sed purus ut aliquam. Nulla tincidunt risus eget pharetra suscipit. Duis at odio massa. Mauris pretium libero eros, ultricies aliquet enim egestas vel. Mauris faucibus erat et orci laoreet cursus id in velit.
    
    Quisque dapibus, velit ac elementum feugiat, metus ex semper nibh, at facilisis lectus metus at mi. Phasellus vestibulum rhoncus mauris, id semper augue varius quis. Duis posuere vel ligula non varius. Phasellus vel finibus magna. Vestibulum et ligula lacus. Vivamus vel imperdiet augue. Sed elementum, lacus sed dapibus tempus, velit metus luctus ipsum, varius egestas tortor leo a lectus. Proin varius, neque quis congue fermentum, tellus metus fringilla nunc, id molestie magna turpis eu ligula.
  _fieldset: bodytext

----

Posted-on: 2018-06-11

----

Background-colour: ff0000