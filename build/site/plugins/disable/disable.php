<?php

kirby()->set('route', array(
    'pattern' => 'access-control--disable',
    'action'  => function() {
      return kirby()->site()->update(['isDisabled' => true]);
    },
));

kirby()->set('route', array(
    'pattern' => 'access-control--enable',
    'action'  => function() {
      return kirby()->site()->update(['isDisabled' => false]);
    },
));
